(function (global, factory) {
  "use strict";
  if ("function" === typeof define && define.amd) {
    define([], function () {
      return (global.oViewer = factory(global));
    });
  } else if ("object" === typeof exports) {
    module.exports = (global.oViewer = factory(global));
  } else {
    global.oViewer = factory(global);
  }
})((("undefined" !== typeof window) ? window : {}), function (window) {
  'use strict';
  var ov = {};
  ov.viewers = [];
  var resourcesList = [{
      name: "fullScreenIcon",
      url: "assets/images/Icons/FullScreen.svg",
    },
    {
      name: "mark",
      url: "assets/images/icons/mark.svg",
    },
    {
      name: "rotatioAllowIcon",
      url: "assets/images/icons/RotatioAllowIcon.svg",
    },
    {
      name: "exitFullScreenIcon",
      url: "assets/images/Icons/ExitFullScreen.svg",
    },
    {
      name: "rotatioLockIcon",
      url: "assets/images/icons/RotationLockIcon.svg",
    }
  ];
  /**
   * Loads resoruces
   * @param {string} url url of the resource
   * @param {function} callback Callback
   */
  var loadResource = function (url, callback) {
    var ajax = new XMLHttpRequest();
    ajax.open("GET", url, true);
    ajax.onreadystatechange = function () {
      if (ajax.readyState != 4 || ajax.status != 200) return;
      callback(ajax.responseText);
    };
    ajax.send();
  };
  var getResource = function (name) {
    var resourcesLenght = resourcesList.length;
    for (var i = 0; i < resourcesLenght; i++) {
      var resource = resourcesList[i];
      if (resource.name === name) {
        return resource.url;
      }
    }
  };
  /**
   * Builds the compass template according to the options
   * @function getCompassTemplate
   * @param {Object} options
   * @return {string} html template
   */
  var getCompassTemplate = function () {
    var arrowIndex = (Array.from(document.querySelectorAll("div.o-viewer"))).length;
    return '<svg  xmlns="http://www.w3.org/2000/svg" class="o-viewer-compass" viewBox="0 0 100 100" fill="#ffffff94" stroke="#ffffff94"><defs><marker id="o-viewer-pointer-arrow' + arrowIndex + '" class="o-viewer-pointer-arrow" markerWidth="100" markerHeight="100" refX="0" refY="40" orient="auto" markerUnits="strokeWidth"><path d="M5,35 23,18  Q 42,40 23,62 L5,45 Q8,40 5,35" stroke-width="1" stroke="none"/></marker></defs><circle cx="50" cy="50" r="45" stroke-width="0" fill="transparent"></circle><circle cx="50" cy="50" r="38"  stroke-width="6" fill="none"></circle><circle cx="50" cy="50" r="6"  stroke-width="0"></circle><path class="o-viewer-pointer" d="M50 50 50 10" stroke-width="1" stroke="transparent" marker-start="url(#o-viewer-pointer-arrow' + arrowIndex + ')"/></svg>';
  };
  // Frames to perfom cover animation
  var markAnimationFrames = [
    "M 7.5 21.5 c -3.3 -1.5 -5.6 -4.7 -6 -8.5 H 0.1 C 0.6 19.2 5.7 24 12 24 h 0.7 l -3.8 -3.8 L 7.5 21.5 Z M 12 0 h -0.7 l 3.8 3.8 l 1.3 -1.3 c 3.3 1.5 5.6 4.7 6 8.5 h 1.5 C 23.4 4.8 18.3 0 12 0 Z",
    "M 9.6 22.2 c -3.5 -0.8 -6.5 -3.4 -7.6 -7.1 l -1.4 0.3 c 1.8 6 7.8 9.6 13.9 8.3 l 0.7 -0.1 l -4.5 -2.9 L 9.6 22.2 Z M 9.5 0.3 L 8.8 0.4 l 4.5 2.9 l 1 -1.5 c 3.5 0.8 6.5 3.4 7.6 7.1 l 1.5 -0.3 C 21.7 2.6 15.7 -1 9.5 0.3 Z",
    "M 11.8 22.5 c -3.6 0 -7 -2 -8.9 -5.3 l -1.3 0.6 c 3 5.5 9.6 7.8 15.3 5.2 l 0.6 -0.3 l -5 -1.9 L 11.8 22.5 Z M 7.1 1 L 6.5 1.3 l 5 1.9 l 0.7 -1.7 c 3.6 0 7 2 8.9 5.3 l 1.4 -0.6 C 19.5 0.8 12.9 -1.5 7.1 1 Z",
    "M 13.9 22.3 c -3.6 0.7 -7.3 -0.5 -9.9 -3.3 L 3 19.8 c 4 4.7 11 5.6 16.1 1.9 l 0.6 -0.4 l -5.3 -0.8 L 13.9 22.3 Z M 4.9 2.3 L 4.4 2.7 l 5.3 0.8 L 10 1.7 c 3.6 -0.7 7.3 0.5 9.9 3.3 L 21 4.2 C 17 -0.5 10 -1.4 4.9 2.3 Z",
    "M 16 21.7 c -3.3 1.4 -7.2 1 -10.3 -1.2 l -0.9 1 c 4.9 3.8 11.9 3.2 16.1 -1.5 l 0.5 -0.5 L 16 19.8 L 16 21.7 Z M 3.1 4 L 2.6 4.5 L 8 4.2 L 7.9 2.4 c 3.3 -1.4 7.2 -1 10.3 1.2 l 1 -1.1 C 14.3 -1.3 7.3 -0.7 3.1 4 Z",
    "M 18 20.6 c -2.9 2.1 -6.9 2.5 -10.4 0.9 l -0.7 1.2 c 5.6 2.7 12.3 0.7 15.5 -4.8 l 0.3 -0.6 l -5.2 1.4 L 18 20.6 Z M 1.6 6 L 1.3 6.6 l 5.2 -1.4 L 6 3.4 c 2.9 -2.1 6.9 -2.5 10.4 -0.9 l 0.8 -1.3 C 11.5 -1.5 4.8 0.5 1.6 6 Z",
    "M 19.6 19.2 c -2.4 2.7 -6.2 3.9 -9.9 3.1 l -0.4 1.3 c 6.1 1.4 12.2 -1.9 14.1 -7.9 l 0.2 -0.7 l -4.8 2.4 L 19.6 19.2 Z M 0.6 8.3 L 0.4 9 l 4.8 -2.4 L 4.3 4.9 c 2.4 -2.7 6.2 -3.9 9.9 -3.1 l 0.5 -1.4 C 8.7 -1.1 2.5 2.3 0.6 8.3 Z",
    "M 21 17.5 c -1.8 3.1 -5.3 5.1 -9.1 5.1 l -0.1 1.4 c 6.2 0.2 11.5 -4.4 12.2 -10.7 l 0.1 -0.7 l -4.2 3.4 L 21 17.5 Z M 0.1 10.7 L 0 11.4 l 4.2 -3.4 L 3 6.6 c 1.8 -3.1 5.3 -5.1 9.1 -5.1 l 0.2 -1.5 C 6 -0.1 0.7 4.5 0.1 10.7 Z",
    "M 21.9 15.5 c -1.1 3.4 -4.1 6.1 -7.8 6.9 l 0.1 1.4 c 6.1 -1.1 10.4 -6.7 9.7 -13 L 23.9 10 l -3.4 4.2 L 21.9 15.5 Z M 0.1 13.3 L 0.1 14 l 3.4 -4.2 L 2.1 8.6 c 1.1 -3.4 4.1 -6.1 7.8 -6.9 L 9.8 0.3 C 3.6 1.4 -0.6 7 0.1 13.3 Z",
    "M 22.4 13.3 c -0.4 3.6 -2.7 6.8 -6.2 8.3 l 0.4 1.3 c 5.7 -2.4 8.7 -8.7 6.8 -14.7 l -0.2 -0.7 l -2.4 4.8 L 22.4 13.3 Z M 0.6 15.7 l 0.2 0.7 l 2.4 -4.8 l -1.6 -0.8 C 2 7.1 4.3 4 7.8 2.4 L 7.4 1 C 1.6 3.4 -1.4 9.7 0.6 15.7 Z",
    "M 22.5 11.1 c 0.4 3.6 -1.3 7.2 -4.4 9.4 l 0.7 1.2 c 5.1 -3.5 6.7 -10.3 3.6 -15.8 L 22 5.4 l -1.4 5.2 L 22.5 11.1 Z M 1.6 18 L 2 18.6 l 1.4 -5.2 l -1.8 -0.5 C 1.2 9.3 2.8 5.7 5.9 3.5 L 5.2 2.2 C 0.1 5.7 -1.5 12.5 1.6 18 Z",
    "M 22.1 9 c 1.1 3.5 0.3 7.3 -2.3 10.1 l 0.9 1 C 25 15.7 25.1 8.7 20.9 4 l -0.5 -0.5 l -0.3 5.4 L 22.1 9 Z M 3.1 20 l 0.5 0.5 l 0.3 -5.4 L 2 15.1 C 0.9 11.6 1.7 7.8 4.3 4.9 l -1 -1.1 C -1 8.3 -1.1 15.3 3.1 20 Z",
    "M 21.2 7 c 1.8 3.2 1.8 7.1 -0.1 10.4 l 1.1 0.8 c 3.2 -5.3 1.9 -12.2 -3.2 -15.9 l -0.6 -0.4 l 0.8 5.3 L 21.2 7 Z M 4.9 21.7 l 0.6 0.4 l -0.8 -5.3 l -1.8 0.3 C 1.1 13.9 1.1 10 3 6.7 L 1.8 5.8 C -1.5 11.1 -0.2 18 4.9 21.7 Z",
    "M 20 5.2 c 2.4 2.7 3.2 6.6 2 10.2 l 1.3 0.6 c 2.1 -5.9 -0.6 -12.3 -6.4 -14.9 l -0.6 -0.3 l 1.9 5 L 20 5.2 Z M 7.1 23 l 0.6 0.3 l -1.9 -5 l -1.7 0.7 c -2.4 -2.7 -3.2 -6.6 -2 -10.2 L 0.7 8.1 C -1.3 13.9 1.4 20.4 7.1 23 Z"
  ];
  /**
   * Creates an options object from an options string
   * @function mapOptions
   * @param {string} optionsString
   * @return {Object} options object
   */
  var mapOptions = function (optsStr) {
    var opts = {}; //Options object
    if (!optsStr) {
      return opts;
    }
    /**
     * Creates the apropiate value from an property string value
     * @function getPropValue
     * @param {string} valueString
     * @return {Object} proper value
     */
    var getPropValue = function (valueStr) {

      // Handle an boolean value
      if (valueStr.toLowerCase() === "true") {
        return true;
      }

      if (valueStr.toLowerCase() === "false") {
        return false;
      }

      // Handle an number value
      if (!isNaN(valueStr)) {
        return parseInt(valueStr);
      }

      // Handle an object value
      var val;

      try {
        val = JSON.parse(valueStr);
        return val;
      } catch (e) {
        // Keep it as string
        return valueStr;
      }
    };

    /**
     * Creates option key on options object
     * @function setOpt
     * @param {string} propertyString
     */
    var setOpt = function (propStr) {
      var propItems = Array.from(propStr.split(":"));
      if (propItems.length === 2 && !!propItems[0] && !!propItems[1]) {
        opts[propItems[0].trim()] = getPropValue(propItems[1]);
      }
    };

    if (!optsStr.includes(';')) { //If there is only one property
      setOpt(optsStr);
      return opts;
    }

    //Handle multiple properties
    var props = Array.from(optsStr.split(';'));
    if (props.length) {
      props.forEach(function (propStr) {
        setOpt(propStr);
      });
    }
    return opts;
  };
  /**
   * Creates a viewer prototype from an image element
   * @function Viewer
   * @param {HTMLImageElement} image
   * @return {Object} options object
   */
  var Viewer = function (image, viewerOptions) {
    var instance = this,
      vw, // Curren canvas
      engine,
      scene, // Current scene
      radius = 50,
      container, // Current container
      compass,
      arrow, // Compass Arrow
      src = image instanceof Image ? image.src : image.toString(), // Image source
      cover,
      fullScreenBtn,
      lockMoveBtn,
      lockedCamera, // Locked camera
      deviceMoveCamera, // Device movement oriented camera
      viewerOptionsList = [],
      stop = true,
      rendered = false;
    /**
     * Gets a 3d babylon point
     * @function getPoint
     * @param {Number} x
     * @param {Number} y
     * @param {Number} z
     * @return {Object} Babylon Vector3
     */
    var getPoint = function (x, y, z) {
      return new BABYLON.Vector3(x, y, z);
    };

    /**
     * Creates a light on the current scene
     * @function createLight
     * @param {Object} light options
     */
    var createLight = function (opts) {
      var light = new BABYLON.HemisphericLight(opts.name, opts.position, scene);
      light.intensity = opts.intensity;
    };

    /**
     * Creates a locked camera on the current scene
     * @function createLockedCamera
     * @param {Object} camera options
     * @return {Object} babylon arc rotate camera
     */
    var createLockedCamera = function (opts) {
      var alpha = -Math.PI / 2,
        beta = Math.PI / 2,
        camera = new BABYLON.ArcRotateCamera(opts.name, alpha, beta, radius, opts.position, scene);

      camera.panningSensibility = opts.sensibility;
      camera.panningAxis = getPoint(0, 0, 0);
      camera.fov = opts.fieldOfView;
      camera.lowerRadiusLimit = radius;
      camera.upperRadiusLimit = radius;
      camera.allowUpsideDown = true;

      return camera;
    };

    /**
     * Creates a device orientation camera on the current scene
     * @function createDeviceMoveCamera
     * @param {Object} camera options
     * @return {Object} babylon deviceOrientationCamera camera
     */
    var createDeviceMoveCamera = function (opts) {
      var camera = new BABYLON.DeviceOrientationCamera(opts.name, getPoint(0, 0, 0), scene);
      camera.setTarget(getPoint(0, 0, 0));
      return camera;
    };

    /**
     * Changes the scene sky enviroment according to the current image source
     * @function createEnviroment
     */
    var createEnviroment = function (callback) {
      var material = new BABYLON.SkyMaterial('material', scene),
        enviroment = BABYLON.Mesh.CreateSphere('enviroment', 32, 1000, scene),
        imageMat = new BABYLON.StandardMaterial('material', scene);

      material.backFaceCulling = false;
      material.useSunPosition = true;
      material.sunPosition = getPoint(0, 20, 500);
      material.rayleigh = 2;
      material.luminance = 1.1;
      enviroment.material = material;
      enviroment.infiniteDistance = true;

      imageMat.diffuseTexture = new BABYLON.Texture(src, scene, false, false, BABYLON.Texture.TRILINEAR_SAMPLINGMODE, callback); //Here's where we change the current image
      imageMat.backFaceCulling = false;
      imageMat.diffuseTexture.coordinatesMode = BABYLON.Texture.FIXED_EQUIRECTANGULAR_MIRRORED_MODE;
      enviroment.material = imageMat;
    };

    /**
     * Orientates the current compass arrow to the given point
     * @function setViewerPointer
     * @param {Number} x
     * @param {Number} y
     */
    var setViewerPointer = function (x, y) {
      y *= -1;
      x += 50;
      y += 50;
      var d = 'M50 50 ' + x + ' ' + y;
      arrow.setAttribute("d", d);
    };

    /**
     * Buils the scene for the current viewer
     * @function buildScene
     */
    var buildScene = function () {

      // Cameras
      lockedCamera = createLockedCamera({
        name: "locked",
        sensibility: 25,
        fieldOfView: 0.9
      });
      lockedCamera.attachControl(vw, true);
      deviceMoveCamera = createDeviceMoveCamera({
        name: "deviceMove",
        sensibility: 25,
        fieldOfView: 0.9
      });
      deviceMoveCamera.attachControl(vw, true);
      scene.activeCamera = lockedCamera; // lockedCamera is default one

      //Lights
      createLight({
        name: "lightUpSideDown",
        position: getPoint(0, -1, 0),
        intensity: 1
      });
      createLight({
        name: "light",
        position: getPoint(0, 1, 0),
        intensity: 1
      });
    };

    var resetCtrlOpts = function (optsName, ctrl) {
      var svg = ctrl.querySelector('svg');

      container.removeAttribute(optsName);
      ctrl.removeAttribute("style");

      if (svg) {
        svg.removeAttribute("style");
      }
    };

    var refreshImage = function () {
      // If the dataset changes the image is changed      
      if (container.dataset.src !== src) {
        src = container.dataset.src;
        if (!(image instanceof Image)) {
          image = new Image();
        }
        image.src = src;
        image.onload = function () {
          if (engine) {
            createEnviroment();
          } else {
            setPreview();
          }
        }
      }
    };
    var getSelectorAttribute = function (attribute) {
      switch (attribute) {
        case 'o-viewer-compass':
        return attribute;          
        case 'o-viewer-fullscreen':
          return 'o-viewer-fs-btn';          
        case 'o-viewer-lockmove':
        return 'o-viewer-lm-btn';          
        case 'o-viewer-mark':
          return attribute;          
      }
    }
    var refreshViewer = function (changes) {
      changes.forEach(function (change) {
        if (change.attributeName.includes('src')) {
          refreshImage();
        } else if (change.attributeName.includes('o-viewer')) {
          var validAttribute = change.attributeName.split('-')[2];
          var ctrl = document.querySelector('.o-viewer-control.' + getSelectorAttribute(change.attributeName));
          if (validAttribute && ctrl) {
            var newOptions = mapOptions(change.target.getAttribute(change.attributeName));
            for (var option in newOptions) {
              applyOption(option, newOptions[option], ctrl);
            }
          }
        }
      });
    }


    var render = function () {
      // Update compass arrow
      if (scene.activeCamera.name === "locked") {
        setViewerPointer(-lockedCamera.position.x, -lockedCamera.position.z);
      } else {
        setViewerPointer(deviceMoveCamera._currentTarget.x, deviceMoveCamera._currentTarget.z);
      }

      scene.render();
      rendered = true;
    }
    /**
     * Keeps updating the viewer
     * @function renderLoop
     */
    var renderLoop = function () {
      if (!rendered) {
        render();
      } else {
        if (!scene || stop) {
          return false;
        }
        render();
      }
    };

    /**
     * Renders the current viewer
     * @function render
     */
    instance.render = function (callback) {
      if (typeof engine === 'undefined') {
        initBabylon();
      }
      buildScene();
      createEnviroment(callback);
      engine.runRenderLoop(renderLoop);
    };

    /**
     * Turns the current viewer to full screen mode
     * @function fullScreen
     */
    var fullScreen = function () {
      var currentValues = container.getAttribute('style');

      container.dataset.beforeFullScreen = currentValues;
      container.removeAttribute('style');


      container.style.position = "absolute";
      container.style.width = "100%";
      container.style.height = "100%";
      container.style.left = "0px";
      container.style.top = "0px";


      if (navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && !navigator.userAgent.match('CriOS')) {
        vw.style.height = "200%";
      }
    };

    /**
     * Recovers the viewer before fullScreen mode
     * @function exitFullScreen
     */
    var exitFullScreen = function () {
      container.removeAttribute('style');
      var currentValues = container.dataset.beforeFullScreen;
      container.setAttribute('style', currentValues);
      container.dataset.beforeFullScreen = '';
      if (navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && !navigator.userAgent.match('CriOS')) {
        vw.style.height = "100%";
      }
    };

    var fakeFullScreen = function () {
      container.classList.add("o-viewer-fullScreen");
      container.dataset.beforeFullScreen = container.getAttribute("style");
      var body = document.body;
      container.removeAttribute("style");
      body.dataset.beforeFullScreen = body.getAttribute("style");
      body.removeAttribute("style");
      window.scrollTo(0, 1);
      body.style.position = "fixed";
      body.style.margin = "0px";
      body.style.height = "100%";
      body.style.width = "100%";
      body.style.overflow = "hidden";
      container.style.position = "absolute";
      container.style.height = body.height + "px";
      container.style.width = body.width + "px";
      container.style.left = "0px";
      container.style.top = "0px";
      container.style.zIndex = 9999;
      engine.resize();
    };

    var exitFakeFullScreen = function () {
      container.classList.remove("o-viewer-fullScreen");
      container.removeAttribute("style");
      container.setAttribute("style", container.dataset.beforeFullScreen);
      container.dataset.beforeFullScreen = "";
      var body = document.body;
      body.removeAttribute("style");
      body.setAttribute("style", body.dataset.beforeFullScreen);
      body.dataset.beforeFullScreen = "";
      engine.resize();
    };

    /**
     * Toggles the viewer between fullScreen and non fullScreen mode
     * @function toggleFullScreen
     */
    var toggleFullScreen = function () {
      if (!document.mozFullScreenElement && !document.webkitFullscreenElement && !container.classList.contains("o-viewer-fullScreen")) { // Has any element on fullScreen state ?
        // Set the current container in fullScreen mode
        if (container.mozRequestFullScreen) { // Try Mozilla
          container.mozRequestFullScreen();
        } else if (container.webkitRequestFullScreen) { // Try WebKit
          container.webkitRequestFullScreen();
        } else {
          fakeFullScreen();
        }

      } else {
        // Take off fullScreen mode
        if (document.mozCancelFullScreen) { // Mozilla
          document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) { // WebKit
          document.webkitCancelFullScreen();
        } else {
          exitFakeFullScreen();
        }
      }

    };

    function drawImage(canvas, image, width, height) {
      width = typeof width === 'number' ? width : canvas.offsetWidth;
      height = typeof height === 'number' ? height : canvas.offsetHeight;
      var context = canvas.getContext('2d');
      context.drawImage(image, width, height, width, height, 0, 0, width / 2.5, height / 3);
    }
    /**
     * Apply a blur effect to a canvas
     * @param {HTMLCanvasElement} canvas 
     * @param {number} strength 
     */
    function blurCanvas(canvas, strength) {
      var context = canvas.getContext('2d'),
        x, y;
      context.globalAlpha = 0.5;
      for (y = -strength; y <= strength; y += 2) {
        for (x = -strength; x <= strength; x += 2) {
          context.drawImage(canvas, x, y);
          if (x >= 0 && y >= 0) {
            context.drawImage(canvas, -(x - 1), -(y - 1));
          }
        }
      }
      context.globalAlpha = 1.0;
    }

    /**
     * Creates the current viewer canvas element
     * @function createCanvas
     */
    var createCanvas = function () {
      return document.createElement('canvas');
    };

    /**
     * @constructor ControlOptions
     */
    function ControlOptions(options) {
      if (!options.ctrl) {
        throw Error('Please provide a control element using options.ctrl');
      }
      var control = this,
        undefinedType = 'undefined',
        colorName = 'color',
        sizeName = 'size',
        showName = 'show',
        yName = 'y',
        xName = 'x';

      control.ctrl = options.ctrl;
      control.getLayoutProp = function (name) {
        var prop = this.ctrl.style[name];
        return prop === "auto" ? "" : prop;
      };

      Object.defineProperty(control, colorName, {
        set: function (newColor) {
          if (typeof newColor !== undefinedType) applyOption(colorName, newColor, control.ctrl);
        },
        get: function () {
          var svg = control.ctrl.querySelector('svg');
          return svg ? svg.style.fill : '';
        }
      });
      Object.defineProperty(control, sizeName, {
        set: function (newSize) {
          if (typeof newSize !== undefinedType) applyOption(sizeName, newSize, control.ctrl);
        },
        get: function () {
          return (window.innerWidth > window.innerHeight) ? control.ctrl.style.width : control.ctrl.style.height;
        }
      });
      Object.defineProperty(control, showName, {
        set: function (_show) {
          if (typeof _show !== undefinedType) applyOption(showName, _show, control.ctrl);
        },
        get: function () {
          return (control.ctrl.style.display !== "none");
        }
      });
      Object.defineProperty(control, yName, {
        set: function (newY) {
          if (typeof newY !== undefinedType) applyOption(yName, newY, control.ctrl);
        },
        get: function () {
          return getLayoutProp('top');
        }
      });
      Object.defineProperty(control, xName, {
        set: function (newX) {
          if (typeof newX !== undefinedType) applyOption(xName, newX, control.ctrl);
        },
        get: function () {
          return getLayoutProp('left');
        }
      });
      control.color = options.color;
      control.size = options.size;
      control.show = options.show;
      control.y = options.y;
      control.x = options.x;
      return control;
    }

    var addInlineAttr = function (ctrl, propName, propVal) {
      if (ctrl && ctrl.optsAttrName) {
        var attrCurrentVal = container.getAttribute(ctrl.optsAttrName);
        attrCurrentVal = attrCurrentVal ? attrCurrentVal : '';
        var regex = new RegExp("(\s+)?" + propName + "(\s+)?\:(\s+)?" + propVal + "(\s+)?\;?");

        if (attrCurrentVal.match(regex)) {
          return;
        }

        if (attrCurrentVal.includes(propName)) {
          regex = new RegExp("(\s+)?" + propName + "(\s+)?\:(\s+)?[^\:\;\n]+(\s+)?\;?");
          attrCurrentVal = attrCurrentVal.replace(regex, propName + ':' + propVal + ';');
          container.setAttribute(ctrl.optsAttrName, attrCurrentVal);
        } else {
          container.setAttribute(ctrl.optsAttrName, attrCurrentVal + propName + ':' + propVal + ';');
        }
      }
    };

    var removeInlineAttr = function (ctrl, propName) {
      if (ctrl && ctrl.optsAttrName && propName) {
        var attrCurrentVal = container.getAttribute(ctrl.optsAttrName);
        if (attrCurrentVal) {
          var regex = new RegExp("(\s+)?" + propName + "(\s+)?\:(\s+)?[^\:\;\n]+(\s+)?\;?");
          var prop = attrCurrentVal.match(regex);
          prop = prop ? prop[0] : '';
          if (prop) {
            container.setAttribute(ctrl.optsAttrName, attrCurrentVal.replace(prop, ''));
          }
        }
      }
    };

    /**
     * Apply options set to the element
     * @function applyOption
     * @param optionName
     * @param value
     * @param control
     */
    var applyOption = function (optionName, value, elm) {
      if (typeof optionName === 'string') { // There are any options
        switch (optionName) {
          case ('show'):
            if (typeof value !== 'undefined') {
              addInlineAttr(elm, 'show', false);
              elm.style.display = 'none';
            } else {
              removeInlineAttr(elm, 'show');
              elm.style.display = '';
            }
            break;
          case ('size'):
            addInlineAttr(elm, 'size', value);
            if(typeof value !== 'string') value = value.toString();
            var sizeNums = value.match(/[\d\.]+/);
            sizeNums = sizeNums ? sizeNums[0] : 0;
            var difference;
            if (container.offsetWidth > container.offsetHeight) { // Landscape view
              difference = container.offsetWidth / container.offsetHeight;
              elm.style.width = value;
              elm.style.height = value.replace(sizeNums.toString(), (sizeNums * difference));
            } else if (container.offsetWidth < container.offsetHeight) { // Portrait view
              difference = container.offsetHeight / container.offsetWidth;
              elm.style.width = value.replace(sizeNums.toString(), (sizeNums * difference));
              elm.style.height = value;
            } else {
              elm.style.width = value;
              elm.style.height = value;
            }
            break;
          case ('color'):
            var svg = elm.querySelector('svg');
            if (svg) {
              addInlineAttr(elm, 'color', value);
              if (svg) {
                svg.style.fill = value;
                if (elm.classList.contains("o-viewer-compass")) {
                  svg.style.stroke = value;
                }
              }
            }
            break;
          case ('y'):
            addInlineAttr(elm, 'y', value);
            elm.style.bottom = "auto";
            elm.style.top = typeof value === 'number' ? value + 'px' : value;
            break;
          case ('x'):
            addInlineAttr(elm, 'x', value);
            elm.style.right = "auto";
            elm.style.left = typeof value === 'number' ? value + 'px' : value;;
            break;
        }
      }
    };

    var getOptKeyNameFromAttrName = function (attrName) {
      if (!attrName) {
        return '';
      }

      attrName = attrName.replace('o-viewer-', '');
      var chunks = attrName.split('-');

      if (chunks.length > 1) {
        chunks.forEach(function (chunk) {
          chunk = chunk.replace(chunk.charAt(0), chunk.charAt(0).toUpperCase());
        });
        return chunks.toString().replace(",", "");
      } else {
        return attrName;
      }
    };

    /**
     * Creates a viewer control
     * @function createViewerControl
     */
    var createViewerControl = function (opts) {
      var optsAttr = image.getAttribute ? image.getAttribute(opts.optsAttr) : "",
        controlOpts,
        ctrl = document.createElement('div');
      ctrl.classList.add('o-viewer-control');

      container.setAttribute(opts.optsAttr, optsAttr ? optsAttr : "");
      var options = viewerOptions ? viewerOptions[getOptKeyNameFromAttrName(opts.optsAttr)] : mapOptions(optsAttr);
      if (!options) {
        options = {};
      }
      options.ctrl = ctrl;
      ctrl.optsAttrName = opts.optsAttr ? opts.optsAttr : "";
      ctrl.optsString = optsAttr ? optsAttr : "";

      if (opts.classes && opts.classes.length) {
        opts.classes.forEach(function (clss) {
          ctrl.classList.add(clss);
        });
      }

      if (opts.icon) {
        loadResource(opts.icon, function (data) {
          var icon = document.createRange().createContextualFragment(data).querySelector("*");
          ctrl.appendChild(icon);
        });
      }

      if (opts.events && opts.events.length) {
        opts.events.forEach(function (e) {
          if (e.type && e.functions && e.functions.length) {
            e.functions.forEach(function (f) {
              ctrl.addEventListener(e.type, f);
            });
          }
        });
      }

      controlOpts = new ControlOptions(options);
      ctrl.controlOpts = controlOpts;

      return ctrl;
    };

    /**
     * Changes the current icon of a control
     * @function changeControlIcon
     */
    var changeControlIcon = function (ctrl, icon) {
      var svg = ctrl.querySelector("svg");
      if (!svg) {
        return;
      }
      var svgStyle = svg.getAttribute("style");
      ctrl.innerHTML = "";
      loadResource(icon, function (data) {
        var newIcon = document.createRange().createContextualFragment(data).querySelector("*");
        newIcon.setAttribute("style", svgStyle);
        ctrl.appendChild(newIcon);
        //applyOptions(ctrl.controlOpts, ctrl);
      });
    };

    var getOptionsStr = function (opts) {
      if (!opts) {
        return '';
      }
      var str = "";
      for (var key in opts) {
        if (opts.hasOwnProperty(key)) {
          str += key + ':' + opts[key] + ';';
        }
      }
      return str;
    };

    var registerOpts = function (attrName, ctrl) {
      viewerOptionsList.push({
        name: attrName,
        val: image.getAttribute ? image.getAttribute(attrName) : viewerOptions ? getOptionsStr(viewerOptions[getOptKeyNameFromAttrName(attrName)]) : "",
        ctrl: ctrl
      });
    };

    /**
     * Creates the current viewer compass element
     * @function createCompass
     */
    var createCompass = function () {
      var _compass = createViewerControl({
        optsAttr: "o-viewer-compass",
        classes: ["o-viewer-compass"]
      });
      _compass.innerHTML = getCompassTemplate();
      registerOpts("o-viewer-compass", _compass);
      return _compass;
    };

    /**
     * Creates the current viewer mark icon element
     * @function createMark
     */
    var createMark = function () {
      var _mark = createViewerControl({
        optsAttr: "o-viewer-mark",
        classes: ["o-viewer-mark"],
        icon: getResource("mark")
      });
      registerOpts("o-viewer-mark", _mark);
      return _mark;
    };

    /**
     * Creates the current viewer fullScreen button element
     * @function createFullScreenBtn
     */
    var createFullScreenBtn = function () {
      var _fs = createViewerControl({
        optsAttr: "o-viewer-fullScreen",
        classes: ["o-viewer-fs-btn", "o-viewer-btn"],
        icon: getResource("fullScreenIcon"),
        events: [{
          type: "click",
          functions: [
            toggleFullScreen
          ]
        }]
      });
      registerOpts("o-viewer-fullScreen", _fs);
      return _fs;
    };

    /**
     * Is executed in order to toggle the active camera
     * @function changeCamera
     */
    var changeCamera = function () {
      if (scene.activeCamera === lockedCamera) {
        scene.activeCamera = deviceMoveCamera;
        changeControlIcon(lockMoveBtn, getResource("rotatioLockIcon"));
      } else {
        scene.activeCamera = lockedCamera;
        changeControlIcon(lockMoveBtn, getResource("rotatioAllowIcon"));
      }
    };

    /**
     * Creates the current viewer lock movement button element
     * @function createLockMovementBtn
     */
    var createLockMovementBtn = function () {
      var _lm = createViewerControl({
        optsAttr: "o-viewer-lockMove",
        classes: ["o-viewer-lm-btn", "o-viewer-btn"],
        icon: getResource("rotatioAllowIcon"),
        events: [{
          type: "click",
          functions: [
            changeCamera
          ]
        }]
      });
      registerOpts("o-viewer-lockMove", _lm);
      return _lm;
    };

    /**
     * Creates the current viewer container element
     * @function createViewerContainer
     */
    var createViewerContainer = function () {
      container = document.createElement('div');
      container.classList.add('o-viewer');
      container.dataset.src = src;
      if (image.id) {
        container.id = image.id;
      }
      // Layout
      if ((image.style && (image.style.width || image.width)) || viewerOptions && (viewerOptions.width || viewerOptions.height)) {
        var width = image.style ? (!!image.style.width ? image.style.width : image.width) : viewerOptions ? viewerOptions.width : "";
        var height = image.style ? (!!image.style.height ? image.style.height : image.height) : viewerOptions ? viewerOptions.height : "";
        if (width) {
          container.style.width = width;
          if (!container.style.width) {
            container.style.width = width + "px";
          }
        }
        if (height) {
          container.style.height = height;
          if (!container.style.height) {
            container.style.height = height + "px";
          }
        }
      }
      return container;
    };

    /**
     * Handles fullScreen mode changes in order to resize the viewer and update the icons
     * @function onFullScreenChange
     */
    var onFullScreenChange = function () {
      if (!container.dataset.beforeFullScreen) {
        container.classList.add("o-viewer-fullScreen");
        fullScreen();
        changeControlIcon(fullScreenBtn, getResource("exitFullScreenIcon"));
      } else {
        container.classList.remove("o-viewer-fullScreen");
        exitFullScreen();
        changeControlIcon(fullScreenBtn, getResource("fullScreenIcon"));
      }
      if (typeof engine !== 'undefined') {
        engine.resize();
      }
    };

    /**
     * Binds fullScreen functions to they element handlers
     * @function bindFullScreenEvents
     */
    var bindFullScreenEvents = function () {
      container.addEventListener("dblclick", toggleFullScreen);
      document.addEventListener("webkitfullscreenchange", onFullScreenChange);
      document.addEventListener("mozfullscreenchange", onFullScreenChange);
    };

    /**
     * Checks if the current device browser is for Mobile or Tablet
     * @function isMobileOrTablet
     */
    var isMobileOrTablet = function () {
      var check = false;
      (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
      })(navigator.userAgent || navigator.vendor || window.opera);
      return check;
    };

    var setPreview = function () {
      drawImage(vw, image);
      blurCanvas(vw, 2);
    };

    /**
     * Builds Viewer elements content and replaces image element
     * @function buildViewer
     */
    var buildViewer = function () {
      container.appendChild(vw);
      container.appendChild(compass);
      container.appendChild(fullScreenBtn);
      container.appendChild(lockMoveBtn);
      container.appendChild(createMark());

      if (!isMobileOrTablet()) {
        lockMoveBtn.controlOpts.show = false;
      }
      if (image.parentNode) {
        image.parentNode.replaceChild(container, image);
      } else {
        if (viewerOptions && viewerOptions.container && viewerOptions.container.appendChild) {
          viewerOptions.container.appendChild(container);
        } else {
          document.body.appendChild(container);
        }
      }
      if (image instanceof Image) {
        if (image.complete) {
          setPreview();
        } else {
          image.onload = setPreview;
        }
      } else {
        image = new Image();
        image.src = src;
        image.onload = setPreview;
      }

    };

    /**
     * Creates the cover screen for when the 360 image is not focused
     * @function createCoverScreen
     */
    var createCoverScreen = function () {
      var attrName = 'o-viewer-cover';

      var opts = image.getAttribute ? image.getAttribute(attrName) : '';

      if (opts) {
        container.setAttribute('o-viewer-cover', opts);
      }

      opts = viewerOptions ? viewerOptions[getOptKeyNameFromAttrName(attrName)] : mapOptions(opts);
      if (!opts) {
        opts = {};
      }

      if (opts.show !== undefined && !opts.show) {
        return;
      }

      cover = document.createElement('div');
      cover.classList.add('o-viewer-cover');

      if (opts.color) {
        cover.style.background = opts.color;
      }

      loadResource(getResource("mark"), function (data) {
        var coverLogo = document.createRange().createContextualFragment(data).querySelector("*");
        coverLogo.classList.add("o-viewer-cover-logo");
        cover.appendChild(coverLogo);
      });


      container.appendChild(cover);
    };

    var loadDependencies = function (callback) {
      var babylonCdn = 'http://localhost:3000/assets/js/libs/babylon.js',
        handCdn = 'https://cdnjs.cloudflare.com/ajax/libs/handjs/1.3.11/hand.min.js',
        injectScript = function (code) {
          eval.call(window, code);
        };
      if (isMobileOrTablet()) {
        loadResource(handCdn, function (data) {
          injectScript(data);
          loadResource(babylonCdn, function (data) {
            injectScript(data);
            callback();
          });
        });
      } else {
        loadResource(babylonCdn, function (data) {
          injectScript(data);
          callback();
        });
      }
    };

    var bindCoverScreenEvents = function () {
      var p,
        animateInterval,
        currentFrame = 0,
        loading = false,
        showCover = function () {
          if (!container.classList.contains("o-viewer-fullScreen")) {
            cover.style.display = "block";
            stop = true;
          }
          currentFrame = 0;
          nextFrame();
        },
        hideCover = function () {
          stopAnimation();
          stop = false;
          cover.style.display = "none";
          loading = false;
        },
        nextFrame = function () {
          if (p) {
            if (!markAnimationFrames[currentFrame]) {
              currentFrame = 0;
            }
            p.setAttribute('d', markAnimationFrames[currentFrame]);
            currentFrame++;
          }
        },
        animate = function () {
          p = cover.querySelector(".o-viewer-mark-arrows");
          animateInterval = setInterval(nextFrame, 50);
        },
        stopAnimation = function () {
          clearInterval(animateInterval);
        },
        clickHandler = function () {
          if (!loading) {
            animate();
            if (typeof BABYLON === 'undefined') {
              loading = true;
              loadDependencies(function () {
                instance.render(hideCover);
              });
            } else if (!rendered) { //Babylon is already loaded
              loading = true;
              instance.render(hideCover);
            } else {
              hideCover();
            }
          }
        },
        documentClickHandler = function (event) {
          if (!container.contains(event.target)) showCover();
        };
      cover.addEventListener("click", clickHandler);
      document.addEventListener("click", documentClickHandler);
      //cover.addEventListener("mouseenter", animate);
      //cover.addEventListener("mouseleave", stopAnimation);
    }

    /**
     * Initializes babylon engine
     * @function buildViewer
     */
    var initBabylon = function () {
      var nvw = createCanvas();
      vw.parentNode.replaceChild(nvw, vw); //Braulio
      vw = nvw;
      // Babylon engine      
      engine = new BABYLON.Engine(vw, true);
      // Creation of the scene
      scene = new BABYLON.Scene(engine);
      scene.clearColor = BABYLON.Color3.White();
    };

    /**
     * Initializes the construction of the viewer
     * @function createViewerElements
     */
    var createViewerElements = function () {
      // Creation of the viewer container and replacement of the image
      createViewerContainer();
      // Creation of the compass
      compass = createCompass();
      arrow = compass.querySelector('.o-viewer-pointer');
      // Creation of the canvas for the scene
      vw = createCanvas();
      //Creation of the controls
      fullScreenBtn = createFullScreenBtn();
      lockMoveBtn = createLockMovementBtn();
      // Merge of elements
      buildViewer();

      var observer = new MutationObserver(refreshViewer);
      observer.observe(container, {
        attributes: true
      });

      // FullScreen events handlers attachment
      bindFullScreenEvents();
      // Init an empty scene

      if (typeof BABYLON !== 'undefined') {
        instance.render();
      }

      // Set the cover screen
      createCoverScreen();
      bindCoverScreenEvents();
    }();
  };

  /**
   * Creates a viewer from an image
   * @function render
   * @param {HTMLImageElement} image
   */
  ov.render = function (vw, opts) {
    var viewer = new Viewer(vw, typeof opts === "object" ? opts : undefined);
    ov.viewers.push(viewer);
    return viewer;
  };

  /**
   * Initializes the module and looks for all images with o-viewer class in order to render them
   * @function init
   */
  var init = function () {
    document.querySelectorAll('img.o-viewer').forEach(ov.render);
  }();

  return ov;
});