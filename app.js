var express = require('express');
var app = express();

//Serve Static JS, CSS or image files
app.use('/', express.static(__dirname + "/"));

//Start HTTP Server
app.listen(3000, () => {
  console.log('3000 ready');
})
